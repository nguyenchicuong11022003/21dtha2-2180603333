﻿namespace Lap4_01
{
    partial class frmQLSV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnSua = new System.Windows.Forms.Button();
            this.cmbKhoa = new System.Windows.Forms.ComboBox();
            this.txtDTB = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtMSSV = new System.Windows.Forms.TextBox();
            this.lblDTB = new System.Windows.Forms.Label();
            this.lblKhoa = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.btnThem = new System.Windows.Forms.Button();
            this.lblMSSV = new System.Windows.Forms.Label();
            this.btnXoa = new System.Windows.Forms.Button();
            this.dgvTTSV = new System.Windows.Forms.DataGridView();
            this.clmMSSV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmKhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grTTSV = new System.Windows.Forms.GroupBox();
            this.lblQLSV = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.chứcNăngToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnQLK = new System.Windows.Forms.ToolStripMenuItem();
            this.TimKiem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTTSV)).BeginInit();
            this.grTTSV.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(838, 403);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 35);
            this.btnThoat.TabIndex = 13;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnSua
            // 
            this.btnSua.Location = new System.Drawing.Point(124, 369);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(75, 35);
            this.btnSua.TabIndex = 11;
            this.btnSua.Text = "Sửa";
            this.btnSua.UseVisualStyleBackColor = true;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // cmbKhoa
            // 
            this.cmbKhoa.FormattingEnabled = true;
            this.cmbKhoa.Location = new System.Drawing.Point(140, 150);
            this.cmbKhoa.Name = "cmbKhoa";
            this.cmbKhoa.Size = new System.Drawing.Size(121, 28);
            this.cmbKhoa.TabIndex = 7;
            // 
            // txtDTB
            // 
            this.txtDTB.Location = new System.Drawing.Point(140, 210);
            this.txtDTB.Name = "txtDTB";
            this.txtDTB.Size = new System.Drawing.Size(108, 26);
            this.txtDTB.TabIndex = 6;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(140, 94);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(166, 26);
            this.txtName.TabIndex = 5;
            // 
            // txtMSSV
            // 
            this.txtMSSV.Location = new System.Drawing.Point(140, 48);
            this.txtMSSV.Name = "txtMSSV";
            this.txtMSSV.Size = new System.Drawing.Size(100, 26);
            this.txtMSSV.TabIndex = 4;
            // 
            // lblDTB
            // 
            this.lblDTB.AutoSize = true;
            this.lblDTB.Location = new System.Drawing.Point(6, 213);
            this.lblDTB.Name = "lblDTB";
            this.lblDTB.Size = new System.Drawing.Size(70, 20);
            this.lblDTB.TabIndex = 3;
            this.lblDTB.Text = "Điểm TB";
            // 
            // lblKhoa
            // 
            this.lblKhoa.AutoSize = true;
            this.lblKhoa.Location = new System.Drawing.Point(6, 150);
            this.lblKhoa.Name = "lblKhoa";
            this.lblKhoa.Size = new System.Drawing.Size(46, 20);
            this.lblKhoa.TabIndex = 2;
            this.lblKhoa.Text = "Khoa";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(6, 94);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(61, 20);
            this.lblName.TabIndex = 1;
            this.lblName.Text = "Họ Tên";
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(12, 369);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(75, 35);
            this.btnThem.TabIndex = 10;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // lblMSSV
            // 
            this.lblMSSV.AutoSize = true;
            this.lblMSSV.Location = new System.Drawing.Point(6, 48);
            this.lblMSSV.Name = "lblMSSV";
            this.lblMSSV.Size = new System.Drawing.Size(81, 20);
            this.lblMSSV.TabIndex = 0;
            this.lblMSSV.Text = "Mã Số SV";
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(243, 369);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 35);
            this.btnXoa.TabIndex = 12;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // dgvTTSV
            // 
            this.dgvTTSV.AllowUserToAddRows = false;
            this.dgvTTSV.AllowUserToDeleteRows = false;
            this.dgvTTSV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTTSV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmMSSV,
            this.clmName,
            this.clmKhoa,
            this.clmDTB});
            this.dgvTTSV.Location = new System.Drawing.Point(388, 72);
            this.dgvTTSV.Name = "dgvTTSV";
            this.dgvTTSV.ReadOnly = true;
            this.dgvTTSV.RowHeadersWidth = 62;
            this.dgvTTSV.RowTemplate.Height = 28;
            this.dgvTTSV.Size = new System.Drawing.Size(596, 323);
            this.dgvTTSV.TabIndex = 9;
            this.dgvTTSV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTTSV_CellClick);
            this.dgvTTSV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTTSV_CellContentClick);
            // 
            // clmMSSV
            // 
            this.clmMSSV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmMSSV.HeaderText = "Mã Số SV";
            this.clmMSSV.MinimumWidth = 8;
            this.clmMSSV.Name = "clmMSSV";
            this.clmMSSV.ReadOnly = true;
            // 
            // clmName
            // 
            this.clmName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmName.HeaderText = "Họ Tên";
            this.clmName.MinimumWidth = 8;
            this.clmName.Name = "clmName";
            this.clmName.ReadOnly = true;
            // 
            // clmKhoa
            // 
            this.clmKhoa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmKhoa.HeaderText = "Tên Khoa";
            this.clmKhoa.MinimumWidth = 8;
            this.clmKhoa.Name = "clmKhoa";
            this.clmKhoa.ReadOnly = true;
            // 
            // clmDTB
            // 
            this.clmDTB.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmDTB.HeaderText = "Điểm TB";
            this.clmDTB.MinimumWidth = 8;
            this.clmDTB.Name = "clmDTB";
            this.clmDTB.ReadOnly = true;
            // 
            // grTTSV
            // 
            this.grTTSV.Controls.Add(this.cmbKhoa);
            this.grTTSV.Controls.Add(this.txtDTB);
            this.grTTSV.Controls.Add(this.txtName);
            this.grTTSV.Controls.Add(this.txtMSSV);
            this.grTTSV.Controls.Add(this.lblDTB);
            this.grTTSV.Controls.Add(this.lblKhoa);
            this.grTTSV.Controls.Add(this.lblName);
            this.grTTSV.Controls.Add(this.lblMSSV);
            this.grTTSV.Location = new System.Drawing.Point(12, 75);
            this.grTTSV.Name = "grTTSV";
            this.grTTSV.Size = new System.Drawing.Size(345, 261);
            this.grTTSV.TabIndex = 8;
            this.grTTSV.TabStop = false;
            this.grTTSV.Text = "Thông Tin Sinh Viên";
            // 
            // lblQLSV
            // 
            this.lblQLSV.AutoSize = true;
            this.lblQLSV.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQLSV.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblQLSV.Location = new System.Drawing.Point(164, -1);
            this.lblQLSV.Name = "lblQLSV";
            this.lblQLSV.Size = new System.Drawing.Size(615, 55);
            this.lblQLSV.TabIndex = 7;
            this.lblQLSV.Text = "Quản lý thông tin sinh viên";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.chứcNăngToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 24);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(996, 33);
            this.menuStrip1.TabIndex = 14;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // chứcNăngToolStripMenuItem
            // 
            this.chứcNăngToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnQLK,
            this.TimKiem});
            this.chứcNăngToolStripMenuItem.Name = "chứcNăngToolStripMenuItem";
            this.chứcNăngToolStripMenuItem.Size = new System.Drawing.Size(113, 29);
            this.chứcNăngToolStripMenuItem.Text = "Chức năng";
            // 
            // btnQLK
            // 
            this.btnQLK.Image = global::Lap4_01.Properties.Resources.user_management_vector_icon;
            this.btnQLK.Name = "btnQLK";
            this.btnQLK.ShortcutKeys = System.Windows.Forms.Keys.F2;
            this.btnQLK.Size = new System.Drawing.Size(254, 34);
            this.btnQLK.Text = "Quản Lý Khoa";
            this.btnQLK.Click += new System.EventHandler(this.btnQLK_Click);
            // 
            // TimKiem
            // 
            this.TimKiem.Image = global::Lap4_01.Properties.Resources.th;
            this.TimKiem.Name = "TimKiem";
            this.TimKiem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.TimKiem.Size = new System.Drawing.Size(254, 34);
            this.TimKiem.Text = "Tìm Kiếm";
            this.TimKiem.Click += new System.EventHandler(this.TimKiem_Click);
            // 
            // menuStrip2
            // 
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(996, 24);
            this.menuStrip2.TabIndex = 15;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // frmQLSV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 450);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.dgvTTSV);
            this.Controls.Add(this.grTTSV);
            this.Controls.Add(this.lblQLSV);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmQLSV";
            this.Text = "Quản Lý Sinh Viên";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTTSV)).EndInit();
            this.grTTSV.ResumeLayout(false);
            this.grTTSV.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.ComboBox cmbKhoa;
        private System.Windows.Forms.TextBox txtDTB;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtMSSV;
        private System.Windows.Forms.Label lblDTB;
        private System.Windows.Forms.Label lblKhoa;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Label lblMSSV;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.DataGridView dgvTTSV;
        private System.Windows.Forms.GroupBox grTTSV;
        private System.Windows.Forms.Label lblQLSV;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMSSV;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmKhoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDTB;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem chứcNăngToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem btnQLK;
        private System.Windows.Forms.ToolStripMenuItem TimKiem;
        private System.Windows.Forms.MenuStrip menuStrip2;
    }
}

