﻿using Lap4_01.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lap4_01
{
    public partial class frmQLSV : Form
    {
        private object reportViewer1;

        public frmQLSV()
        {
            InitializeComponent();
           
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                StudentContextDB context = new StudentContextDB();

                List<Faculty> listFacultys = context.Faculty.ToList();
                List<Student> listStudent = context.Student.ToList();
                FillFacultyCombobox(listFacultys);
                BindGrid(listStudent);
            }
            catch (Exception ex)

            {
                MessageBox.Show(ex.Message);
            }
        }
        private void LoadData()
        {
            StudentContextDB context = new StudentContextDB();
            List<Student> listStudent = context.Student.ToList();
            BindGrid(listStudent);

        }
            private void FillFacultyCombobox(List<Faculty> listFacultys)
            {
                this.cmbKhoa.DataSource = listFacultys;
                this.cmbKhoa.DisplayMember = "FacultyName";
                this.cmbKhoa.ValueMember = "FacultyID";
            }
            private void BindGrid(List<Student> listStudent)
            {
                dgvTTSV.Rows.Clear();
                foreach (var item in listStudent)
                {
                    int index = dgvTTSV.Rows.Add();
                    dgvTTSV.Rows[index].Cells[0].Value = item.StudentID;
                    dgvTTSV.Rows[index].Cells[1].Value = item.FullName;
                    dgvTTSV.Rows[index].Cells[2].Value = item.Faculty.FacultyName;
                    dgvTTSV.Rows[index].Cells[3].Value = item.AverageScore;
                }
            }

        private void btnThem_Click(object sender, EventArgs e)
        {
            StudentContextDB context = new StudentContextDB();
            string studentID = txtMSSV.Text;
            string HoTen = txtName.Text;
            double dtb = Convert.ToDouble(txtDTB.Text);
            int maKhoa = Convert.ToInt32(cmbKhoa.SelectedValue.ToString());

            if (txtMSSV.Text == "" || txtName.Text == "" || txtDTB.Text == "")
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thong tin!");
            } else if (txtMSSV.Text.Length < 10)
            {
                MessageBox.Show("MSSV phải đủ 10 kí tự!");
            }else
            {
                Student st = new Student();
                st.StudentID = studentID;
                st.FullName = HoTen;
                st.AverageScore = dtb;
                st.FacultyID = maKhoa;

                context.Student.Add(st);
                context.SaveChanges();
                
                MessageBox.Show("Thêm dữ liệu thành công!");
            }
            LoadData();
            begin();

        }
        private  void begin()
        {
            txtMSSV.Text = "";
            txtName.Text = "";
            txtDTB.Text = "";
            cmbKhoa.SelectedValue = 0;

        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            StudentContextDB context = new StudentContextDB();
            string studentID = txtMSSV.Text;
            string HoTen = txtName.Text;
            double dtb = Convert.ToDouble(txtDTB.Text);
            int maKhoa = Convert.ToInt32(cmbKhoa.SelectedValue.ToString());
            Student KtraSV = context.Student.FirstOrDefault(s  => s.StudentID == studentID);
            if (KtraSV == null)
            {
                MessageBox.Show("Không tìm thấy  MSSV cần sửa!");
            }
            else
            {
                KtraSV.FullName = HoTen;
                KtraSV.AverageScore = dtb;
                KtraSV.FacultyID = maKhoa;

                context.SaveChanges();
                MessageBox.Show("Cập nhập thông tin thành công!");
            }
            LoadData();
            begin();

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            StudentContextDB context = new StudentContextDB();
            string studentID = txtMSSV.Text;
            string HoTen = txtName.Text;
            double dtb = Convert.ToDouble(txtDTB.Text);
            int maKhoa = Convert.ToInt32(cmbKhoa.SelectedValue.ToString());
            Student KtraSV = context.Student.FirstOrDefault(s => s.StudentID == studentID);
            if (KtraSV == null)
            {
                MessageBox.Show("Không tìm thấy  MSSV cần xóa!");
            }
            else
            {
                DialogResult result = MessageBox.Show("Bạn có chắc muốn xóa sinh viên này!","Thông Báo",MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    context.Student.Remove(KtraSV);
                    context.SaveChanges();
                }
            }
            LoadData();
            begin();


        }

        private void dgvTTSV_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex    = e.RowIndex;
            txtMSSV.Text = dgvTTSV.Rows[rowIndex].Cells[0].Value.ToString();
            txtName.Text = dgvTTSV.Rows[rowIndex].Cells[1].Value.ToString();
            cmbKhoa.Text = dgvTTSV.Rows[rowIndex].Cells[2].Value.ToString();
            txtDTB.Text = dgvTTSV.Rows[rowIndex].Cells[3].Value.ToString();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnQLK_Click(object sender, EventArgs e)
        {
            FrmFalculty from = new FrmFalculty();
            from.ShowDialog();
        }

        private void dgvTTSV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void TimKiem_Click(object sender, EventArgs e)
        {
            FrmSearch from = new FrmSearch();
            from.ShowDialog();
        }
    }
}
