﻿using Lap4_01.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Lap4_01
{
    public partial class FrmFalculty : Form
    {
        public FrmFalculty()
        {
            InitializeComponent();
        }

        private void FrmFalculty_Load(object sender, EventArgs e)
        {
            try
            {
                StudentContextDB context = new StudentContextDB();

                List<Faculty> listFacultys = context.Faculty.ToList();
                BindGrid(listFacultys);
            }
            catch (Exception ex)

            {
                MessageBox.Show(ex.Message);
            }

        }
        private void BindGrid(List<Faculty> listFacultys)
        {
            dgvTTK.Rows.Clear();
            foreach (var item in listFacultys)
            {
                int index = dgvTTK.Rows.Add();
                dgvTTK.Rows[index].Cells[0].Value = item.FacultyID.ToString();
                dgvTTK.Rows[index].Cells[1].Value = item.FacultyName;
                dgvTTK.Rows[index].Cells[2].Value = item.TotalProfessor.ToString();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThem_Click(object sender, EventArgs e)
        {
            StudentContextDB context = new StudentContextDB();
            int facultyID = Convert.ToInt32(txtMK.Text);
            string FacultyName = txtTK.Text;
            int TGS = Convert.ToInt32(txtTGS.Text);
            if(txtMK.Text == ""|| txtTK.Text == "")
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thong tin ma khoa va ten khoa!");
            } else
            {
                Faculty f = new Faculty();
                f.FacultyID = facultyID;
                f.FacultyName = FacultyName;
                f.TotalProfessor = TGS;
                context.Faculty.Add(f);
                context.SaveChanges();
            }
            LoadData();
            begin();

        }
        private void begin()
        {
            txtMK.Text = "";
            txtTK.Text = "";
            txtTGS.Text = "";
        }
        private void LoadData()
        {
            StudentContextDB context = new StudentContextDB();
            List<Faculty> listFacultys = context.Faculty.ToList();
            BindGrid(listFacultys);

        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            StudentContextDB context = new StudentContextDB();
            int facultyID = Convert.ToInt32(txtMK.Text);
            string FacultyName = txtTK.Text;
            int TGS = Convert.ToInt32(txtTGS.Text);
            Faculty KtraMK = context.Faculty.FirstOrDefault(s => s.FacultyID == facultyID);
            if(KtraMK == null)
            {
                MessageBox.Show("Không tìm thấy  Ma khoa cần sửa!");
            } else
            {
                KtraMK.FacultyName = FacultyName;
                KtraMK.TotalProfessor = TGS;

                context.SaveChanges();
                MessageBox.Show("Cập nhập thông tin thành công!");
            }
            LoadData();
            begin();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            StudentContextDB context = new StudentContextDB();
            int facultyID = Convert.ToInt32(txtMK.Text);
            string FacultyName = txtTK.Text;
            int TGS = Convert.ToInt32(txtTGS.Text);
            Faculty KtraMK = context.Faculty.FirstOrDefault(s => s.FacultyID == facultyID);
            if (KtraMK == null)
            {
                MessageBox.Show("Không tìm thấy  Ma khoa cần xoa!");
            } else
            {
                DialogResult result = MessageBox.Show("Bạn có chắc muốn xóa khoa này!", "Thông Báo", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    context.Faculty.Remove(KtraMK);
                    context.SaveChanges();
                }
            }
            LoadData();
            begin();

        }

        private void dgvTTK_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int selectedRow;
            if (e.RowIndex >= 0)
            {
                selectedRow = e.RowIndex;
                DataGridViewRow dgv = dgvTTK.Rows[selectedRow];
                txtMK.Text = dgv.Cells[0].Value.ToString();
                txtTK.Text = dgv.Cells[1].Value.ToString();
                txtTGS.Text = dgv.Cells[2].Value.ToString();
            }
        }

      
    }
}
