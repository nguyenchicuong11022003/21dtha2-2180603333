﻿namespace Lap4_01
{
    partial class FrmFalculty
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grTTK = new System.Windows.Forms.GroupBox();
            this.txtTGS = new System.Windows.Forms.TextBox();
            this.txtTK = new System.Windows.Forms.TextBox();
            this.txtMK = new System.Windows.Forms.TextBox();
            this.lblTongGS = new System.Windows.Forms.Label();
            this.lblTenKhoa = new System.Windows.Forms.Label();
            this.lblMK = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnThem = new System.Windows.Forms.Button();
            this.btnSua = new System.Windows.Forms.Button();
            this.btnXoa = new System.Windows.Forms.Button();
            this.dgvTTK = new System.Windows.Forms.DataGridView();
            this.clmMK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTenKhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTongGS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnThoat = new System.Windows.Forms.Button();
            this.grTTK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTTK)).BeginInit();
            this.SuspendLayout();
            // 
            // grTTK
            // 
            this.grTTK.Controls.Add(this.txtTGS);
            this.grTTK.Controls.Add(this.txtTK);
            this.grTTK.Controls.Add(this.txtMK);
            this.grTTK.Controls.Add(this.lblTongGS);
            this.grTTK.Controls.Add(this.lblTenKhoa);
            this.grTTK.Controls.Add(this.lblMK);
            this.grTTK.Location = new System.Drawing.Point(12, 59);
            this.grTTK.Name = "grTTK";
            this.grTTK.Size = new System.Drawing.Size(279, 273);
            this.grTTK.TabIndex = 0;
            this.grTTK.TabStop = false;
            this.grTTK.Text = "Thông Tin Khoa";
            // 
            // txtTGS
            // 
            this.txtTGS.Location = new System.Drawing.Point(117, 168);
            this.txtTGS.Name = "txtTGS";
            this.txtTGS.Size = new System.Drawing.Size(100, 26);
            this.txtTGS.TabIndex = 5;
            // 
            // txtTK
            // 
            this.txtTK.Location = new System.Drawing.Point(117, 104);
            this.txtTK.Name = "txtTK";
            this.txtTK.Size = new System.Drawing.Size(152, 26);
            this.txtTK.TabIndex = 4;
            // 
            // txtMK
            // 
            this.txtMK.Location = new System.Drawing.Point(117, 55);
            this.txtMK.Name = "txtMK";
            this.txtMK.Size = new System.Drawing.Size(100, 26);
            this.txtMK.TabIndex = 1;
            // 
            // lblTongGS
            // 
            this.lblTongGS.AutoSize = true;
            this.lblTongGS.Location = new System.Drawing.Point(6, 171);
            this.lblTongGS.Name = "lblTongGS";
            this.lblTongGS.Size = new System.Drawing.Size(73, 20);
            this.lblTongGS.TabIndex = 3;
            this.lblTongGS.Text = "Tổng GS";
            // 
            // lblTenKhoa
            // 
            this.lblTenKhoa.AutoSize = true;
            this.lblTenKhoa.Location = new System.Drawing.Point(6, 110);
            this.lblTenKhoa.Name = "lblTenKhoa";
            this.lblTenKhoa.Size = new System.Drawing.Size(77, 20);
            this.lblTenKhoa.TabIndex = 2;
            this.lblTenKhoa.Text = "Tên Khoa";
            // 
            // lblMK
            // 
            this.lblMK.AutoSize = true;
            this.lblMK.Location = new System.Drawing.Point(11, 55);
            this.lblMK.Name = "lblMK";
            this.lblMK.Size = new System.Drawing.Size(72, 20);
            this.lblMK.TabIndex = 1;
            this.lblMK.Text = "Mã Khoa";
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(12, 364);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(75, 27);
            this.btnThem.TabIndex = 1;
            this.btnThem.Text = "Thêm";
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // btnSua
            // 
            this.btnSua.Location = new System.Drawing.Point(106, 364);
            this.btnSua.Name = "btnSua";
            this.btnSua.Size = new System.Drawing.Size(75, 27);
            this.btnSua.TabIndex = 2;
            this.btnSua.Text = "Sửa";
            this.btnSua.UseVisualStyleBackColor = true;
            this.btnSua.Click += new System.EventHandler(this.btnSua_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(206, 364);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 27);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // dgvTTK
            // 
            this.dgvTTK.AllowUserToAddRows = false;
            this.dgvTTK.AllowUserToDeleteRows = false;
            this.dgvTTK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTTK.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmMK,
            this.clmTenKhoa,
            this.clmTongGS});
            this.dgvTTK.Location = new System.Drawing.Point(297, 76);
            this.dgvTTK.Name = "dgvTTK";
            this.dgvTTK.ReadOnly = true;
            this.dgvTTK.RowHeadersWidth = 62;
            this.dgvTTK.RowTemplate.Height = 28;
            this.dgvTTK.Size = new System.Drawing.Size(491, 311);
            this.dgvTTK.TabIndex = 4;
            this.dgvTTK.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTTK_CellClick);
         
            // 
            // clmMK
            // 
            this.clmMK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmMK.HeaderText = "Mã Khoa";
            this.clmMK.MinimumWidth = 8;
            this.clmMK.Name = "clmMK";
            this.clmMK.ReadOnly = true;
            // 
            // clmTenKhoa
            // 
            this.clmTenKhoa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmTenKhoa.HeaderText = "Tên Khoa";
            this.clmTenKhoa.MinimumWidth = 8;
            this.clmTenKhoa.Name = "clmTenKhoa";
            this.clmTenKhoa.ReadOnly = true;
            // 
            // clmTongGS
            // 
            this.clmTongGS.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmTongGS.HeaderText = "Tổng số GS";
            this.clmTongGS.MinimumWidth = 8;
            this.clmTongGS.Name = "clmTongGS";
            this.clmTongGS.ReadOnly = true;
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(678, 406);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 32);
            this.btnThoat.TabIndex = 5;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // FrmFalculty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.dgvTTK);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnSua);
            this.Controls.Add(this.btnThem);
            this.Controls.Add(this.grTTK);
            this.Name = "FrmFalculty";
            this.Text = "Quản Lý Khoa";
            this.Load += new System.EventHandler(this.FrmFalculty_Load);
            this.grTTK.ResumeLayout(false);
            this.grTTK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTTK)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grTTK;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.TextBox txtTGS;
        private System.Windows.Forms.TextBox txtTK;
        private System.Windows.Forms.TextBox txtMK;
        private System.Windows.Forms.Label lblTongGS;
        private System.Windows.Forms.Label lblTenKhoa;
        private System.Windows.Forms.Label lblMK;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Button btnSua;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.DataGridView dgvTTK;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMK;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTenKhoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTongGS;
        private System.Windows.Forms.Button btnThoat;
    }
}