﻿using Lap4_01.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lap4_01
{
    public partial class FrmSearch : Form
    {
        public FrmSearch()
        {
            InitializeComponent();
            
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            StudentContextDB context = new StudentContextDB();
            string studentID = txtMSSV.Text.Trim();
            string fullname = txtName.Text.Trim();
            int facultyID = Convert.ToInt32(cmbKhoa.SelectedValue);

            IQueryable<Student> students = context.Student.Include("Faculty");
            if(!string.IsNullOrEmpty(studentID) )
            {
                students = students.Where(s => s.StudentID == studentID);
            }
            if(!string.IsNullOrEmpty(fullname) )
            {
                students = students.Where(s => s.FullName == fullname);
            }
            if(facultyID != 0)
            {
                students = students.Where(s => s.FacultyID == facultyID);
            }
            List<Student> result = students.ToList();
            dgvSearch.Rows.Clear();
            foreach(var student in result)
            {
                int index = dgvSearch.Rows.Add();
                dgvSearch.Rows[index].Cells[0].Value = student.StudentID;
                dgvSearch.Rows[index].Cells[1].Value = student.FullName;
                dgvSearch.Rows[index].Cells[2].Value = student.AverageScore;
                dgvSearch.Rows[index].Cells[3].Value = student.Faculty.FacultyName;
            }
            int tongKetQua = result.Count();
            txtKQ.Text = tongKetQua.ToString();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            txtMSSV.Text = "";
            txtName.Text = "";
            cmbKhoa.SelectedIndex = 0;
            dgvSearch.Rows.Clear();
            txtKQ.Text = "";
        }
        private void OnTextChanged(object sender, EventArgs e)
        {
            btnTimKiem_Click(sender, e);
        }
        private void FillFacultyCombobox(List<Faculty> listFacultys)
        {
            this.cmbKhoa.DataSource = listFacultys;
            this.cmbKhoa.DisplayMember = "FacultyName";
            this.cmbKhoa.ValueMember = "FacultyID";
        }

        private void FrmSearch_Load(object sender, EventArgs e)
        {
            StudentContextDB context = new StudentContextDB();
            try
            {
                List<Faculty> listFacultys = context.Faculty.ToList();
                FillFacultyCombobox(listFacultys);
                txtMSSV.TextChanged += OnTextChanged;
                txtName.TextChanged += OnTextChanged;

                cmbKhoa.SelectedIndexChanged += OnTextChanged;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnTV_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
