﻿namespace Lap4_01.Migrations
{
    using Lap4_01.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Lap4_01.Model.StudentContextDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Lap4_01.Model.StudentContextDB context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            context.Faculty.AddOrUpdate(
     f => f.FacultyID,
     new Faculty { FacultyID = 1, FacultyName = "Công nghệ thông tin" },
     new Faculty { FacultyID = 2, FacultyName = "Quản trị kinh doanh" }
 );

            // Thêm một số Students
            context.Student.AddOrUpdate(
                s => s.StudentID,
                new Student { StudentID = "S1", FullName = "Student1", AverageScore = 8.5, FacultyID = 1 },
                new Student { StudentID = "S2", FullName = "Student2", AverageScore = 9.0, FacultyID = 2 }
            );

            context.SaveChanges();
        }
    }
}
