﻿namespace Lap4_01.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    

    public partial class UpdateDatabase : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Faculty",
                c => new
                    {
                        FacultyID = c.Int(nullable: false),
                        FacultyName = c.String(nullable: false, maxLength: 200),
                        TotalProfessor = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FacultyID);
            
            CreateTable(
                "dbo.Student",
                c => new
                    {
                        StudentID = c.String(nullable: false, maxLength: 20),
                        FullName = c.String(nullable: false, maxLength: 200),
                        AverageScore = c.Double(nullable: false),
                        FacultyID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.StudentID)
                .ForeignKey("dbo.Faculty", t => t.FacultyID)
                .Index(t => t.FacultyID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Student", "FacultyID", "dbo.Faculty");
            DropIndex("dbo.Student", new[] { "FacultyID" });
            DropTable("dbo.Student");
            DropTable("dbo.Faculty");
        }
    }
}
